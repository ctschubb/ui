import { sampleTemplatePage } from './app.po';

describe('sample App', function() {
  let page: sampleTemplatePage;

  beforeEach(() => {
    page = new sampleTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
